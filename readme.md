## Alt Banano Hunter

Chrome extension to hunt some Alts on Discord bot, soon maybe telegram.

---

## How to add this chrome extension

It's a private addon so, you will download as zip on your computer then unzip, so go to chrome (or Brave or any chromium browser):

- Open your settings

<img src="path-path/banano-1.png" width="200"/>

- Settings page in the side menu you'll see Extensions area.

<img src="path-path/banano-2.png" width="200"/>

- On the extensions page you'll need to use Developer mode, just check on right side

<img src="path-path/banano-2.5.png" width="200">

- Now you'll can choose unpacked addon and select the unziped folder from this project.

<img src="path-path/banano-3.png" width="500"/>

- After install as unpacked, you can use it on Banano Looker:

<img src="path-path/banano-4.png" width="200"/>

- Just on click on the button than will show inside an account the username from banano bot.

<img src="path-path/banano-6.png" width="400">

---

All the updates will need to do the same steps as before, and I will advise when new version comes, please suggestions are welcome.
