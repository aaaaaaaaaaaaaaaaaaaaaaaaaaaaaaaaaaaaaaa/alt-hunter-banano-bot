let arrayAddress = [];
let userAccount;
let isCreeper = window.location.href.includes("creeper");
let isLooker = window.location.href.includes("looker");
const clickedEvent = new CustomEvent("clickOnAddressEvent");

const getDataByKirbyApi = (item) => {
	let data;
	const findAddress = arrayAddress.filter(
		(banAddress) => banAddress.address === item.textContent
	);
	data = findAddress;
	return data;
};

const getFirstAddress = async () => {
	if (isCreeper) {
		const address = document.querySelector(".text-monospace");
		const getAccountTitle = document.querySelector("h1.mb-0");
		let data = await fetchDataFromDiscordApi(address.textContent);
		if (data.length === 0) {
			data = getDataByKirbyApi(address);
		}
		getAccountTitle.innerHTML = `${getAccountTitle.textContent
			} <span style="font-size:23px">${data.length > 0
				? (data[0].user_last_known_name &&
					`Discord User: ${data[0].user_last_known_name}`) ||
				data[0].owner
				: "Unknown Address"
			}</span>`;
	}
};

const fetchAll = async (newDiv) => {
	newDiv.forEach((div) => {
		div.addEventListener("click", (e) => {
			window.dispatchEvent(clickedEvent);
		});
	});
	newDiv.forEach(async (item) => {
		item.setAttribute("data-read", "true");
		let data = await fetchDataFromDiscordApi(item.textContent);
		if (data.length === 0) {
			data = getDataByKirbyApi(item);
		}
		item.innerHTML = `${item.textContent} = <b>${data.length > 0
			? (data[0].user_last_known_name &&
				`Discord User: ${data[0].user_last_known_name}`) ||
			data[0].owner
			: "Unknown Address"
			}</b>`;
	});
};

const fetchDatafromKirbyApi = async () => {
	const response = await fetch(
		"https://kirby.eu.pythonanywhere.com/api/v1/resources/addresses/all"
	);
	const data = await response.json();
	arrayAddress = data;
};

const fetchDataFromDiscordApi = async (address) => {
	const response = await fetch(`https://bananobotapi.banano.cc/ufw/${address}`);
	const data = await response.json();
	return data;
};

const container = document.querySelector("#root");

var observer = new MutationObserver((_, observer) => {
	let selectors = [];
	if (isLooker) {
		selectors.push(".break-word.color-normal");
		selectors.push(".ant-btn:not(.ant-btn-circle),[class^='ant-pagination-item']");
	}
	if (isCreeper) {
		selectors.push(".text-dark.break-word");
		selectors.push(".btn.btn-nano-primary");
	}

	if (selectors.length > 0) {
		let bananoAddressContainer = document.querySelectorAll(selectors[0]);
		let buttonGet_list = document.querySelectorAll(selectors[1]);
		if (bananoAddressContainer) {
			bananoAddressContainer.forEach((div) => {
				div.addEventListener("click", (e) => {
					window.dispatchEvent(clickedEvent);
				});
			});
		}
		if (buttonGet_list.length > 0) {
			buttonGet_list.forEach((buttonGet) => {
				buttonGet.addEventListener("click", (e) => {
					e.preventDefault();
					setTimeout(() => {
						let updated;
						if (buttonGet_list.length > 1) {
							updated = document.querySelectorAll(selectors[0]);
						} else {
							updated = document.querySelectorAll(selectors[0] + ":not([data-read=true])");
						};
						fetchAll(updated);
					}, 1500);
				});
			});
		}

		if (bananoAddressContainer.length > 0 && buttonGet_list.length > 0) {
			getFirstAddress();
			fetchAll(bananoAddressContainer);
			observer.disconnect();
		}
	}
});

window.addEventListener("clickOnAddressEvent", (e) => {
	fetchDatafromKirbyApi();
	observer.observe(container, {
		childList: true,
		subtree: true,
	});
});

fetchDatafromKirbyApi();
observer.observe(container, {
	childList: true,
	subtree: true,
});